from django.db import models

PASS = "PASS"
PROGRESS = "PROGRESS"
FAIL = "FAIL"
SCHEDULED = "SCHEDULED"
STATUS = [
    (PASS, "Pass"),
    (PROGRESS, "In Progress"),
    (SCHEDULED, "Scheduled"),
    (FAIL, "Failed")
]
# Create your models here.


class RawMatter(models.Model):
    code = models.CharField(max_length=255, unique=True, primary_key=True)
    name = models.CharField(max_length=1024)

    def __str__(self):
        return "[%s] %s" % (self.code, self.name)


class Product(models.Model):
    code = models.CharField(max_length=255, unique=True, primary_key=True)
    name = models.CharField(max_length=1024)
    recipe = models.ManyToManyField('RawMatter', blank=True)

    def __str__(self):
        return "[%s] %s" % (self.code, self.name)


class QualityTest(models.Model):
    order_id = models.CharField(max_length=1024, unique=True, primary_key=True)
    name = models.CharField(max_length=255)
    product = models.ForeignKey('Product', models.CASCADE)
    quantity = models.IntegerField(default=0)
    date = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=255, choices=STATUS, default=SCHEDULED)
    notes = models.TextField(max_length=2048, blank=True)
    close = models.BooleanField(default=False)

    def __str__(self):
        return "%s - %s" % (self.name, self.date)


class StepGroup(models.Model):
    order = models.IntegerField()
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class StepInfo(models.Model):
    order = models.IntegerField()
    group = models.ForeignKey('StepGroup', models.CASCADE)
    name = models.CharField(max_length=255)
    description = models.TextField(max_length=2048, blank=True)

    def __str__(self):
        return "[%s] %s" % (self.group.name, self.name)


class QualityStep(models.Model):
    step = models.ForeignKey('StepInfo', models.CASCADE)
    quality_test = models.ForeignKey('QualityTest', models.CASCADE)
    notes = models.TextField(max_length=2048, blank=True, null=True)
    status = models.CharField(max_length=255, choices=STATUS, default=SCHEDULED)

    def __str__(self):
        return "%s %s" % (self.step.name, self.quality_test.name)
