from django.contrib import admin

# Register your models here.
from main_app.models import RawMatter, Product, QualityTest, StepGroup, StepInfo, QualityStep

admin.site.register(RawMatter)
admin.site.register(Product)
admin.site.register(QualityTest)
admin.site.register(StepGroup)
admin.site.register(StepInfo)
admin.site.register(QualityStep)
