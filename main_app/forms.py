from django import forms
from main_app.models import QualityStep, QualityTest


class QualityStepForm(forms.ModelForm):

    notes = forms.CharField(widget=forms.Textarea(attrs={"class": "materialize-textarea"}), required=False)

    class Meta:
        model = QualityStep
        fields = ['notes', 'status']


class QualityTestForm(forms.ModelForm):

    notes = forms.CharField(widget=forms.Textarea(attrs={"class": "materialize-textarea"}), required=False)

    class Meta:
        model = QualityTest
        fields = ['notes', 'status']
