from json import dumps
from rest_framework import authentication, status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_api_key.permissions import HasAPIKey

from main_app.models import QualityTest, Product, StepInfo, QualityStep, RawMatter


class QualityTestApi(APIView):

    http_method_names = ['post', 'get', 'head']
    authentication_classes = [authentication.SessionAuthentication, authentication.TokenAuthentication]
    permission_classes = [HasAPIKey | IsAuthenticated]

    def head(self, request, *args, **kwargs):
        return Response({"data": "OK"}, status=status.HTTP_200_OK)

    def get(self, request, *args, **kwargs):
        pk = kwargs.get('pk', None)
        if pk:
            data = QualityTest.objects.get(pk=pk)
            return Response(self.get_data(data))
        else:
            data = QualityTest.objects.all()
            return Response([{d.id: self.get_data(d)} for d in data])

    def post(self, request, *args,**kwargs):
        product_code = request.data.get('product', None)
        quantity = request.data.get('quantity', None)
        order_id = request.data.get('orderId', None)
        if not product_code or not quantity or not order_id:
            return Response({"data": "Not OK", "error": "missing argument"}, status=status.HTTP_400_BAD_REQUEST)
        product = Product.objects.get(code=product_code)
        name = "[%s] %s" % (order_id, product.name)
        new_test = QualityTest.objects.create(name=name, product=product, order_id=order_id, quantity=quantity)
        new_test.save()
        steps_info = StepInfo.objects.all()
        for step_info in steps_info:
            step = QualityStep(step=step_info, quality_test=new_test)
            step.save()
        return Response(self.get_data(new_test))

    def get_data(self, data):
        return {
            "name": data.name,
            "orderId": data.order_id,
            "quantity": data.quantity,
            "product": {
                "name": data.product.name,
                "code": data.product.code
            },
            "date": data.date,
            "status": data.status,
            "notes": data.notes
        }


class ProductApi(APIView):

    http_method_names = ['post', 'get', 'head']
    authentication_classes = [authentication.SessionAuthentication, authentication.TokenAuthentication]
    permission_classes = [HasAPIKey | IsAuthenticated]

    def head(self, request, *args, **kwargs):
        return Response({"data": "HEAD OK"}, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        name = request.data.get('Nom', None)
        code = request.data.get('Code', None)
        recipe = request.data.get('Recette', None)

        product, created = Product.objects.get_or_create(name=name, code=str(code))

        for raw in recipe:
            raw_matter, created = RawMatter.objects.get_or_create(
                name=raw['Matiere-Premiere']["Nom"],
                code=str(raw['Matiere-Premiere']["Code"])
            )
            product.recipe.add(raw_matter)
            product.save()

        return Response(dumps({
            "name": product.name,
            "code": product.code,
            "recipe": [{"name": r.name, "code": r.code} for r in product.recipe.all()]
        }), status=status.HTTP_200_OK)
