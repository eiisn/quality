import json
import requests
from django.conf import settings
from django.shortcuts import render, redirect
from main_app.forms import QualityTestForm, QualityStepForm
from main_app.models import QualityTest, PASS, FAIL
# Create your views here.


def home(request):
    if request.user.is_anonymous:
        return redirect("login")
    quality_test = QualityTest.objects.all()
    return render(request, 'main_app/home.html', {
        "quality_test": quality_test
    })


def quality_test_update(request, test_pk):
    if request.user.is_authenticated:
        quality_test = QualityTest.objects.get(pk=test_pk)
        steps = quality_test.qualitystep_set.all()
        context = {
            "steps": steps,
            "steps_groups": [],
            "quality_test": quality_test,
            "quality_test_form": None,
            "message": []
        }
        if request.method == 'GET' or quality_test.close:
            context["quality_test_form"] = QualityTestForm(instance=quality_test, prefix='quality-test')
        elif request.method == 'POST':
            context["quality_test_form"] = QualityTestForm(request.POST, instance=quality_test, prefix='quality-test')
            if context["quality_test_form"].is_valid():
                quality_test.status = context["quality_test_form"].cleaned_data.get('status')
                quality_test.notes = context["quality_test_form"].cleaned_data.get('notes')
                quality_test.save()
                context["message"].append("Quality Test saved")

        for step in context["steps"]:
            if request.method == 'GET' or quality_test.close:
                step.form = QualityStepForm(instance=step, prefix=str(step.id))
            elif request.method == 'POST':
                step.form = QualityStepForm(request.POST, instance=step, prefix=str(step.id))
                if step.form.is_valid():
                    step.status = step.form.cleaned_data.get('status')
                    step.notes = step.form.cleaned_data.get('notes')
                    step.save()
                    context["message"].append("Step %s saved" % step.step.name)
            if step.step.group not in context["steps_groups"]:
                context["steps_groups"].append(step.step.group)

        if request.method == 'POST' and not quality_test.close:
            if quality_test.status in [PASS, FAIL]:
                quality_test.close = True
                quality_test.save()
                esb_url = settings.ESB_URL_QUALITY_VALIDATION
                api_token = settings.ESB_API_TOKEN
                headers = {
                    "X-Quality-Client-Token": api_token,
                }
                payload = {
                    "orderId": quality_test.order_id,
                    "product": {
                        "code": quality_test.product.code,
                        "name": quality_test.product.name
                    },
                    "status": quality_test.status,
                    "quantity": quality_test.quantity
                }
                r = requests.post(esb_url, data=json.dumps(payload), headers=headers)
                context["message"].append(str(r.text))
        context["message"] = json.dumps(context["message"])
        return render(request, 'main_app/quality_test.html', context)
    return redirect("login")
