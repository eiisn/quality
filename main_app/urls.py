"""Quality URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from main_app.api import *
from main_app.views import *


urlpatterns = [
    path("", home, name="home"),
    path("test/<str:test_pk>/", quality_test_update, name="test"),
    path("api/quality/", QualityTestApi.as_view(), name="test-api"),
    path("api/quality/<str:pk>/", QualityTestApi.as_view(), name="test-api"),
    path("api/product/", ProductApi.as_view(), name="product")
]
