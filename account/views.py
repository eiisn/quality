from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect


# Create your views here.


def login_view(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return redirect('home')
        else:
            return render(request, 'account/login.html', {'error': 'Wrong credentials'})
    else:
        return render(request, 'account/login.html')


def logout_view(request):
    if not request.user.is_anonymous:
        logout(request)
        return render(request, 'account/login.html', {'info': 'Disconnected by user'})
    else:
        return redirect("login")
