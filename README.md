# Etapes à simuler :
> 1. Validation de la proposition
> 2. Collecte et gestion des documents
> 3. Analyse et validations des données (à la main > humain)
> 4. Profils toxicologique
> 5. Calculs des marges de sécurité 
>   1. Test d'odeur
>   2. Réaction cutanée
>   3. Tests de fuite du packaging
>   4. Tests fonctionnels
>   5. Vérifications des finitions
> 6. Signature du rapport de sécurité (à la main > humain)
> 7. Génération d'étiquette conforme
> 8. Compilation du DIP (dossier d'information produit)
> 9. Notification CPNP (à la main > humain)
> 10. Veille réglementaire

# Liens utiles :
> - [Etapes de validation](https://www.ecomundo.eu/fr/blog/cosmetiques-mise-marche-5-etapes)
